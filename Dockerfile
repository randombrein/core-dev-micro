FROM registry.gitlab.com/randombrein/core-dev-micro:premake
MAINTAINER Evren KANALICI <evren.kanalici@nevalabs.com>

ENV DEBIAN_FRONTEND noninteractive


####################
# projects
####################
# COPY core-filter/               /nevalabs/core-filter/
# COPY image-editor-filter/       /nevalabs/image-editor-filter/
# COPY image-resize-filter/       /nevalabs/image-resize-filter/
# COPY periodic-filter/           /nevalabs/eriodic-filter/
# COPY video-tamper-detection/    /nevalabs/video-tamper-detection/


# RUN cd /nevalabs/core-filter/               && premake5 gmake && cd build/gmake && make config=release_x64
# RUN cd /nevalabs/image-editor-filter/       && premake5 gmake && cd build/gmake && make config=release_x64
# RUN cd /nevalabs/image-resize-filter/       && premake5 gmake && cd build/gmake && make config=release_x64
# RUN cd /nevalabs/eriodic-filter/            && premake5 gmake && cd build/gmake && make config=release_x64
# RUN cd /nevalabs/video-tamper-detection/    && premake5 gmake && cd build/gmake && make config=release_x64


ENTRYPOINT /bin/bash
